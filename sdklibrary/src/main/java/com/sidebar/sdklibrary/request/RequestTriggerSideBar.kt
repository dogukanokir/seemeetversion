package com.sidebar.sdklibrary.request

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.JsonElement
import com.sidebar.sdklibrary.network.*

class RequestTriggerSideBar(activity: AppCompatActivity?, fragment: Fragment?, name: String?, groups: String?, listener: NetworkResponseListener<JsonElement>) {


    init {

        val request = RequestCreator.create<Services.GetTriggerSideBar>(Services.GetTriggerSideBar::class.java, Constants.NETWORKADDRESS.sideBarUrl)
        request.getTriggerSideBar(name,groups).enqueue(NetworkResponse(listener, 1, activity, fragment))

    }

}