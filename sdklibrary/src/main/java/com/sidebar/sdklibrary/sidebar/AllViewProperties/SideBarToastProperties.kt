package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.Typeface

data class SideBarToastProperties(var toastBackgroundColor: Int?,var titleSubTitleTypeface: Typeface?,var subTitleColor: Int?,var closeIconColor: Int?,var strokeColor: Int?)