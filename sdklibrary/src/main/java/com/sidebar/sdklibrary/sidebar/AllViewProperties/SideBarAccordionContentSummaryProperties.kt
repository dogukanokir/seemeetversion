package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.Typeface

class SideBarAccordionContentSummaryProperties(var titleColor: Int?, var titleTypeface: Typeface?, var subTitleColor: Int?, var subTitleTypeface: Typeface?, var lineViewColor: Int?,var titleTextSize: Float?,var subTitleTextSize: Float?)