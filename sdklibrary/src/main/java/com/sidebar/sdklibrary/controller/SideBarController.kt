package com.sidebar.sdklibrary.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.sidebar.sdklibrary.network.NetworkResponseListener
import com.sidebar.sdklibrary.request.RequestSideBar
import com.sidebar.sdklibrary.request.RequestTriggerSideBar

class SideBarController(private val activity: AppCompatActivity?, private val fragment: Fragment?, private val apiKey: String?,var groups: String?,private val showPlace: Int?,private val parentCol: Long?,private val exists: Boolean?,private val filter: JsonObject?,private val status: Int?,private val order: String?,private val site: Int?, private var sideBarListener: SideBarListener?,private var  triggerSideBarListener: TriggerSideBarListener?) {


    fun sideBar(){

        RequestSideBar(activity,fragment,apiKey,groups,showPlace,parentCol,exists,filter,status,order,site,object :
            NetworkResponseListener<JsonArray> {
            override fun onResponseReceived(response: JsonArray) {
                sideBarListener?.sideBarListener(true,response,-1,"")
            }

            override fun onEmptyResponse(response: JsonArray?) {
                sideBarListener?.sideBarListener(true,null,-1,"")
            }

            override fun onError(failMessage: Int, error: String) {
                sideBarListener?.sideBarListener(false,null,failMessage,error)
            }

        })

    }

    fun triggerSideBar(name: String?, groups: String?){

        RequestTriggerSideBar(activity,fragment,name,groups, object : NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                triggerSideBarListener?.triggerSideBarListener(true,response,-1,"")
            }

            override fun onEmptyResponse(response: JsonElement?) {
                triggerSideBarListener?.triggerSideBarListener(true,null,-1,"")
            }

            override fun onError(failMessage: Int, error: String) {
                triggerSideBarListener?.triggerSideBarListener(false,null,-1,"")
            }


        })

    }

    interface SideBarListener {
        fun sideBarListener(responseOk: Boolean, jsonArray: JsonArray?, failMessage: Int, error: String)
    }

    interface TriggerSideBarListener {
        fun triggerSideBarListener(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int, error: String)
    }



}