package com.sidebar.sdklibrary.response


import com.google.gson.annotations.SerializedName


class TriggerSideBarResponse {
    @SerializedName("@context")
    var context: String? = null
    @SerializedName("hydra:member")
    var hydraMember: List<HydraMemberTrigger>? = null
    @SerializedName("hydra:search")
    var hydraSearch: HydraSearch? = null
    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null
    @SerializedName("hydra:view")
    var hydraView: HydraView? = null
    @SerializedName("@id")
    var id: String? = null
    @SerializedName("@type")
    var type: String? = null
}

class Result{

    @SerializedName("content")
    var content: String? = null
    @SerializedName("@id")
    var id: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("@type")
    var type: String? = null
    @SerializedName("typeStr")
    var typeStr: String? = null
    @SerializedName("viewTypeStr")
    var viewTypeStr: String? = null
    @SerializedName("linkAndroid")
    var linkAndroid: String? = null

}


class HydraMemberTrigger {
    @SerializedName("events")
    var events: List<String>? = null
    @SerializedName("@id")
    var id: String? = null
    @SerializedName("result")
    var result: Result? = null
    @SerializedName("@type")
    var type: String? = null
}
