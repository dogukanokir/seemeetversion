package com.sidebar.sdklibrary.network

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


class ConnectivityInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())

    }
}
